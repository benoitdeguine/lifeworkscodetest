//
//  BoardingCard.swift
//  LifeWorksCodeTest
//
//  Created by Benoit Deguine on 29/06/2018.
//  Copyright © 2018 Benoit Deguine. All rights reserved.
//

import Foundation

class BoardingCard:JSONDecodable {
    
    var from:String
    var to:String
    var steps:[Journey]
    
    required init(decoder: JSONDecoder) throws {
        self.from = try decoder.decode(key: "from")
        self.to = try decoder.decode(key: "to")
        self.steps = try decoder.decode(key: "steps")
    }
    
}
