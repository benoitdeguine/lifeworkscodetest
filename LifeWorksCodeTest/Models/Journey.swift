//
//  Journey.swift
//  LifeWorksCodeTest
//
//  Created by Benoit Deguine on 29/06/2018.
//  Copyright © 2018 Benoit Deguine. All rights reserved.
//

import Foundation

class Journey:JSONDecodable {
    
    var from:String
    var to:String
    var step:Int
    
    required init(decoder: JSONDecoder) throws {
        self.from = try decoder.decode(key: "from")
        self.to = try decoder.decode(key: "to")
        self.step = try decoder.decode(key: "step")
    }
    
}
