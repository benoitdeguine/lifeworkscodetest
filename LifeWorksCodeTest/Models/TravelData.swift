//
//  TravelData.swift
//  LifeWorksCodeTest
//
//  Created by Benoit Deguine on 29/06/2018.
//  Copyright © 2018 Benoit Deguine. All rights reserved.
//

import Foundation

class TravelData:JSONDecodable {
    
    var travels:[BoardingCard]

    required init(decoder: JSONDecoder) throws {
        self.travels = try decoder.decode(key: "travels")
    }
    
}
