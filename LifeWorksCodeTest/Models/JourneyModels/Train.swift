//
//  Train.swift
//  LifeWorksCodeTest
//
//  Created by Benoit Deguine on 29/06/2018.
//  Copyright © 2018 Benoit Deguine. All rights reserved.
//

import Foundation

class Train:Journey {
    
    var number:String
    var seat:String
    
    required init(decoder: JSONDecoder) throws {
        self.number = try decoder.decode(key: "number")
        self.seat = try decoder.decode(key: "seat")

        try super.init(decoder: decoder)
    }
 
}
