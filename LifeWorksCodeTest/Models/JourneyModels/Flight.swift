//
//  Flight.swift
//  LifeWorksCodeTest
//
//  Created by Benoit Deguine on 29/06/2018.
//  Copyright © 2018 Benoit Deguine. All rights reserved.
//

import Foundation

class Flight:Journey {
    
    var number:String
    var gate:String
    var baggages:[Baggage]
    
    required init(decoder: JSONDecoder) throws {
        self.number = try decoder.decode(key: "number")
        self.gate = try decoder.decode(key: "gate")
        self.baggages = try decoder.decode(key: "baggages")
        
        try super.init(decoder: decoder)
    }
    
}
