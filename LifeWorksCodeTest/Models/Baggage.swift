//
//  Baggage.swift
//  LifeWorksCodeTest
//
//  Created by Benoit Deguine on 29/06/2018.
//  Copyright © 2018 Benoit Deguine. All rights reserved.
//

import Foundation

class Baggage:JSONDecodable {
    
    var number:String
    var comment:String
    
    required init(decoder: JSONDecoder) throws {
        self.number = try decoder.decode(key: "number")
        self.comment = try decoder.decode(key: "comment")
    }
    
}
