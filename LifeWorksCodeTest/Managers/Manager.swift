//
//  Manager.swift
//  LifeWorksCodeTest
//
//  Created by Benoit Deguine on 29/06/2018.
//  Copyright © 2018 Benoit Deguine. All rights reserved.
//

import Foundation

enum DataManagerError: Error {
    
    case unknown
    case invalidResponse
    case noFile
    
}

class DataManager {
    
    typealias ManagerDataCompletion = (TravelData?, DataManagerError?) -> ()
    
    func getTravel(completion: @escaping ManagerDataCompletion) {
        
        DispatchQueue.main.async {
            let data = self.loadJSON(withName: "BoardingCards", extension: "json")
            let travelData:TravelData = try! JSONDecoder.decode(data: data)
            
            completion(travelData, nil)
        }
        
    }
    
    private func loadJSON(withName name: String, extension: String) -> Data {
        let bundle = Bundle.main
        let url = bundle.url(forResource: name, withExtension: `extension`)
        
        return try! Data(contentsOf: url!)
    }
    
}
