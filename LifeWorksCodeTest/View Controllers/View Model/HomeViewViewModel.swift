//
//  HomeViewViewModel.swift
//  LifeWorksCodeTest
//
//  Created by Benoit Deguine on 29/06/2018.
//  Copyright © 2018 Benoit Deguine. All rights reserved.
//

import Foundation

struct HomeViewViewModel {
    
    let journey: Journey
    
    var to: String {
        return journey.to
    }
    
    var from:String {
        return journey.from
    }
    
    var text:String {
        return (from) + " ▶ " + to
    }
    
}
