//
//  HomeTableViewCell.swift
//  LifeWorksCodeTest
//
//  Created by Benoit Deguine on 30/06/2018.
//  Copyright © 2018 Benoit Deguine. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(model:HomeViewViewModel) {
        self.textLabel?.text = model.text
    }
    
}
