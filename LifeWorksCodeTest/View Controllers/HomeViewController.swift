//
//  ViewController.swift
//  LifeWorksCodeTest
//
//  Created by Benoit Deguine on 29/06/2018.
//  Copyright © 2018 Benoit Deguine. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    var travels = [Journey]() {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sortArray: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell")
        
        DataManager().getTravel { (travelData, error) in
            
            guard let fromCity = travelData?.travels.first?.from, let toCity = travelData?.travels.first?.to else {
                return
            }
            
            self.title = fromCity + " ▶ " + toCity
            
            if let travels = travelData?.travels.first?.steps {
                self.travels = travels
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func sortMyArray(_ sender: Any) {
        self.travels = self.travels.sorted(by: { $1.step > $0.step })
    }
    
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: HomeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as? HomeTableViewCell
        {
            let homeViewViewModel = HomeViewViewModel(journey: self.travels[indexPath.row])
            cell.configure(model: homeViewViewModel)
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.travels.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Unsorted trips"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
}
