//
//  HomeLifeWorksCodeTestTests.swift
//  LifeWorksCodeTestTests
//
//  Created by Benoit Deguine on 29/06/2018.
//  Copyright © 2018 Benoit Deguine. All rights reserved.
//

import XCTest
@testable import LifeWorksCodeTest

class HomeLifeWorksCodeTestTests: XCTestCase {
    
    var travelData:TravelData?
    
    override func setUp() {
        super.setUp()
        
        let data = loadFileFromBundle(withName: "BoardingCards", extension: "json")
        self.travelData = try! JSONDecoder.decode(data: data)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testGetFirstDestinationStepUnordered() {
        if let to:String = travelData?.travels.first?.steps.first?.to {
            XCTAssertEqual(to, "London")
        }
    }
    
    func testGetFirstDestinationStepOrdered() {
        
        let arraySorted = self.travelData?.travels.first?.steps.sorted(by: { $1.step > $0.step })
        
        if let to:String = arraySorted?.first?.to {
            XCTAssertEqual(to, "Barcelona")
        }
    }
    
    
    
}
